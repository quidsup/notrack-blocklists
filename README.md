# NoTrack Blocklist is Closing
Unfortunately, I no longer have time to keep maintaining the NoTrack blocklist.  
It will be closed for new submissions from 5th May 2023 and move to support only for the next few months.  

New domains will only be added from what my automation scripts find, or what I personally add.  

I have enjoyed disrupting the data collection of so many trackers, and hopefully others will continue the same cause for years to come.  
_Quids_

## Merge Requests
Please do not use merge requests in order to add/remove domains from this blocklist.  
The list is stored in a SQL table and is automatically uploaded to GitLab once per day. Any changes have to be made in the SQL data.

## Stale Domains
All domains in the blocklist are periodically checked for traffic volumes via OpenDNS API. If the volume of traffic reaches a very low figure, then it’s indicative that the domain is no longer serving a purpose, and will automatically be removed from NoTrack blocklists. 

## Donations
Maintaining the NoTrack blocklist does take a lot of time and I would appreciate any small donation. [PayPal](https://paypal.me/quidsup)

## Other Projects
[NoTrack Annoyance Blocklist](https://gitlab.com/quidsup/notrack-annoyance-blocklist) Contains a list of annoying domains, such as Cookie Popups, which fall outside of Tracking and Advertising.
[NoTrack](https://gitlab.com/quidsup/notrack) Network-wide DNS server which blocks Tracking and Advertising sites.

## License
NoTrack blocklists is licensed under [GNU General Puplic License v3.0](https://gitlab.com/quidsup/notrack-blocklists/-/blob/master/LICENSE)  
I'm happy for NoTrack blocklists to be linked to in other projects.
